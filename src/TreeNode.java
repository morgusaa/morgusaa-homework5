
import java.util.*;

public class TreeNode {

   private String name;
   private TreeNode firstChild;
   private TreeNode nextSibling;

   TreeNode (String n, TreeNode d, TreeNode r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
	   
   }
   
   public void setName(String s){
	   this.name = s;
   }
   
   public void setFirstChild(TreeNode d){
	   this.firstChild = d;
   }
   
   public void setNextSibling(TreeNode r){
	   this.nextSibling = r;
   }
   
   public static TreeNode parsePrefix (String s) {
	   int openParenthesisCounter = 0;
	   int closeParenthesisCounter = 0;

	   TreeNode result = new TreeNode(null, null, null);
	   
	   StringBuilder stringBuilder = new StringBuilder();
	   StringTokenizer stringTokenizer = new StringTokenizer(s, "(),", true);
	   
	   if (s.isEmpty()){
		   throw new RuntimeException("Etteantud sõne on tühi.");
	   }
	   
	   
	   
	   String[] characterList = s.trim().split("");
	   
	   for (int i = 0; i < characterList.length; i++){
		   if (characterList[i].equals("(")){
			   openParenthesisCounter = openParenthesisCounter + 1;
			   if (characterList[i-1].equals("(") || characterList[i+1].equals("(")){
				   throw new RuntimeException("Sõnes on järjest samasugused sulud.  Sõne: " + s);
			   }
			   if (characterList[i+1].equals(")")){
				   throw new RuntimeException("Sõnes on tühjad sulud.  Sõne: " + s);
			   }
		   }
		   if (characterList[i].equals(")")){
			   closeParenthesisCounter = closeParenthesisCounter + 1;
//			   if (characterList[i-1].equals(")")){
//				   throw new RuntimeException("Sõnes on järjest samasugused sulud.  Sõne: " + s);
//			   }
			   
		   }
		   if (characterList[i].equals(",")){
			   // i-1 equals ) puudub, sest sulgeva sulu järel võib koma olla.
			   if (characterList[i-1].equals("(") || characterList[i+1].equals(")") || characterList[i+1].equals("(")){
				   throw new RuntimeException("Sõnes on koma valel kohal.  Sõne: " + s);
			   }
			   
			   if (characterList[i-1].equals(",") || characterList[i+1].equals(",")){
				   throw new RuntimeException("Sõnes on mitu koma järjest. Sõne: " + s);
			   }
			   
		   }
		   
	   }
	   
	   if (openParenthesisCounter != closeParenthesisCounter){
		   throw new RuntimeException("Etteantud sõnel on sulgudega probleem. Avavaid sulge: " + openParenthesisCounter + 
				   ". Sulgevaid sulge: " + closeParenthesisCounter + ". Sõne: " + s);
	   }
	   	   
	   if (openParenthesisCounter == 0 || closeParenthesisCounter == 0){
		   throw new RuntimeException("Etteantud sõnes pole korrektset sulupaari. Sõne: " + s);
	   }
	   
	   // Alustame parse'imisega.
	   
	   for (int i = 0; i < s.length(); i++){
		   // Ootame puu algust.
		   if (s.charAt(i) == '('){
			   if (stringBuilder.length() > 0){
				   result.setName(stringBuilder.toString());
				   System.out.println("Rooti nimi on: " + result.name);
				   
				   result.setFirstChild(parseFurther(s.substring(i + 1)));
				   System.out.println("Esimese lapse nimi on: " + result.firstChild.name);
				   stringBuilder.setLength(0);
			   }
		   } else if (s.charAt(i) == ','){
			   result.setName(stringBuilder.toString());
			   stringBuilder.setLength(0);
			   result.setNextSibling(parseFurther(s.substring(i + 1)));
		   } else if (s.charAt(i) == ')'){

		   } else {
			   // Järelikult täht, nimi läheb pikemaks.
			   stringBuilder.append(s.charAt(i));
		   }
	   }   
	   
	   
      return result;  // TODO!!! return the root
   }
   
   
   public static TreeNode parseFurther(String s){
	   TreeNode result = new TreeNode(null, null, null);
	   
	   StringBuilder stringBuilder = new StringBuilder();
	   
	   for (int i = 0; i < s.length(); i++){
		   if (s.charAt(i) == '('){
			   if (stringBuilder.length() > 0){
				   result.setName(stringBuilder.toString());
				   System.out.println("Alamtulemi nimi on " + result.name);
				   stringBuilder.setLength(0);
				   result.setFirstChild(parseFurther(s.substring(i+1)));
			   } else {
				   throw new RuntimeException("Etteantud sõnes on kaks avanevat sulgu järjest: " + s);
			   }
		   } else if (s.charAt(i) == ','){
			   result.setName(stringBuilder.toString());
			   stringBuilder.setLength(0);
			   result.setNextSibling(parseFurther(s.substring(i+1)));
			   System.out.println("Järgmise siblingu nimi on: " + result.nextSibling.name);
			   
		   } else if (s.charAt(i) == ')'){
			   System.out.println("Siin on resulti tagastamine.");
			   return result;
		   } else {
			   // Järelikult täht, nimi läheb pikemaks.
			   stringBuilder.append(s.charAt(i));
		   }
	   }
	   
	   
	   
	   return result;
   }

   public String rightParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      
      if (this.firstChild != null){
    	  b.append("(");
    	  b.append(this.firstChild.rightParentheticRepresentation());
    	  b.append(")");
      }
      
      b.append(this.name);
      
      if (this.nextSibling != null){
    	  b.append(",");
    	  b.append(this.nextSibling.rightParentheticRepresentation());
      }
      
      return b.toString();
   }

   public static void main (String[] param) {
      String s = "A(B1,C,D)";      
      TreeNode t = TreeNode.parsePrefix (s);
      
      // rightParentheticReprenstation toimib.
//      TreeNode D = new TreeNode("D", null, null);
//      TreeNode C = new TreeNode("C", null, D);
//      TreeNode B = new TreeNode("B1", null, C);
//      TreeNode t = new TreeNode("A", B , null);

      String v = t.rightParentheticRepresentation();
      System.out.println (s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
   }
}

